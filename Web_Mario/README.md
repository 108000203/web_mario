# Software Studio 2021 Spring Assignment 2


## Basic Components
|Component|Score|Y/N|
|:-:|:-:|:-:|
|Membership Mechanism|10%|Y|
|Complete Game Process|5%|Y|
|Basic Rules|45%|Y|
|Animations|10%|Y|
|Sound Effects|10%|Y|
|UI|10%|Y|

## Website Detail Description
https://web-mario-e6c40.web.app/

![](https://i.imgur.com/ldpssfQ.jpg)


# Basic Components Description : 
1. World map : level1(forest)、level2(desert), user can select map before game start.
Forest
![](https://i.imgur.com/aOvJDwN.jpg)
Desert
![](https://i.imgur.com/uuib1xI.jpg)



3. Player : user can control player(mario) by keyboard. press "z" to move left ; press "x" to move right ; press "k" to jump. Mario will get hurt if it hits enemies without  hitting enemies' head and will die immediately if it falls out of the map.
![](https://i.imgur.com/rqXeXS0.jpg)

5. Enemies : 3 different enemies(AngelGoomba、Turtle、PipeFlower), player can kill enemies by jump on their head(except PipeFlower). 
AngelGoomba
![](https://i.imgur.com/ylNzDvQ.jpg)
Turtle
![](https://i.imgur.com/gyVs7hN.jpg)
PipeFlower
![](https://i.imgur.com/Mq3wzok.jpg)




7. Question Blocks : 2 different question blocks(coin、 ushroom). When mario hit a coin question block, add 100 point to player. When mario hit a mushroom question block, it will generate a mushroom.
Coin question block
![](https://i.imgur.com/JOuoa1H.jpg)
Mushroom question block
![](https://i.imgur.com/S6yyZV8.jpg)



9. Animations : player、enemies and question boxes each has its own moving animation.
10. Sound effects : For player(jump、stomp、powerup、powerdown、loseonelife、gameover)
11. UI : login/sign button、email/username/password input、level select button、timer display、life display、score display、back bottun、leaderboard button、leaderboard layout 
login inputs
![](https://i.imgur.com/hyCKm7t.jpg)
signin inputs
![](https://i.imgur.com/Jv1nG10.jpg)
level select button
![](https://i.imgur.com/wxe332x.jpg)
timer、life、score display
![](https://i.imgur.com/6AJpnEO.jpg)
back button、leaderboad button
![](https://i.imgur.com/n9oMxcQ.jpg)





# Bonus Functions Description : 
1. LeaderBoard: show the top 10 score records in history.
![](https://i.imgur.com/ubXCPAB.jpg)

3. Flying-Mode : press "f" making mario able to successively jump, like flying!
4. Pause Game : press "p" to pause game, and press again to resume game.
5. Menu animation : in menu page, title sprite will scale up and down with the tempo of background music! (deploy後，需要在起始畫面空白處空點滑鼠一下才會開始播放音樂)
6. Recent OnLine Time & score : recall you the last updated record by your previous log-in. (紀錄玩家上一次登入的最後一筆分數跟遊玩日期) 
![](https://i.imgur.com/UmIvStV.jpg)

8. Play Guide : introduce how to play.
![](https://i.imgur.com/75BChW2.jpg)

10. Weighted Score : players will receive a weighted score after level clear, which is the multiple of left time and score that players have got in game.


12. Web Notification : browser will send user a web notification if user is successfully log in.
13. Enjoy the game!

