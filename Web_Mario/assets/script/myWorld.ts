
const {ccclass, property} = cc._decorator;

@ccclass
export default class CWorld extends cc.Component {

    @property()
    WorldFallG: number = 0;    

    @property() 
    WorldWalkA: number = 0;

    @property({type:cc.AudioClip})
    bgm: cc.AudioClip = null;

    @property({type:cc.AudioClip})
    gameClearBGM: cc.AudioClip = null;

    @property({type:cc.AudioClip})
    gameOverBGM: cc.AudioClip = null;

    @property({type:cc.AudioClip})
    gameFinishBGM: cc.AudioClip = null;

    private pDown : boolean = false;

    static G: number = 0;    
    static WalkA: number = 0; 

    private currentLevel: number = 1;
    
    private life: number = null;

    private timer: number = 60;

    private PlayerData: cc.Component = null;

    private score: number = 0;
    

    
    
    // LIFE-CYCLE CALLBACKS:

    onLoad () {
        CWorld.G = this.WorldFallG;    
        CWorld.WalkA = this.WorldWalkA; 

        cc.director.getPhysicsManager().gravity = cc.v2(0, -200);
        cc.director.getPhysicsManager().enabled = true;

        cc.systemEvent.on(cc.SystemEvent.EventType.KEY_DOWN, this.onKeyDown, this);
        cc.systemEvent.on(cc.SystemEvent.EventType.KEY_UP, this.onKeyUp, this);

        // game web window hide
        cc.game.on(cc.game.EVENT_HIDE, function () {
            cc.log("hide");
            cc.audioEngine.pauseMusic();
            cc.audioEngine.pauseAllEffects();
        });
        // global player data
        this.life = cc.find("Player Commander").getComponent("PlayerCommander").playerLife;
        cc.log("onload life: " + this.life);

        cc.director.getCollisionManager().enabled = true;
        cc.director.getCollisionManager().enabledDebugDraw = true;
        cc.director.getCollisionManager().enabledDrawBoundingBox = true;



    }

    start () {

        //cc.director.loadScene("game_prepare")
        // enable Collision System
        
        

        cc.audioEngine.setMusicVolume(0.5);
        cc.audioEngine.playMusic(this.bgm, true);

        // set Timer
        cc.log("---start timer---");
        this.schedule(()=>{
            this.setTimer();    
        }, 1);

        // display current life on game boarder
        cc.find("Canvas/Main Camera/GameBoarder/Lifer/Life").getComponent(cc.Label).string = String(this.life);

    }

    // update (dt) {}

    onKeyDown(event) {
        // pause the game
        if(event.keyCode == cc.macro.KEY.p) {
            
            if(!this.pDown){
                cc.log("pause");
                this.pDown = true;
                cc.game.pause();
            } else if(this.pDown){
                cc.log("resume");
                this.pDown = false;
                cc.game.resume();
            }
                
        }
    }
    
    onKeyUp(event) {

        
            
    }

    setTimer(){
        this.timer--;
        //cc.log(this.timer);
        cc.find("Canvas/Main Camera/GameBoarder/Timer/leftTime").getComponent(cc.Label).string = String(this.timer);
        if(this.timer == 0){
            this.gameOver();
        }
    }

    // when player reach the goal flag
    gameClear(){


        cc.log("---game clear---")
        cc.audioEngine.playMusic(this.gameClearBGM, false);
        cc.director.getPhysicsManager().enabled = false;
        
        cc.game.removePersistRootNode(cc.find("Player Commander"));

        this.unscheduleAllCallbacks();
        
        cc.find("Canvas/Main Camera/Dialog").getComponent("Dialog").showDialog();

        // Update LeaderBoard
        cc.log("---updating update LeaderBoard---")
        let leaderBoardRef = firebase.database().ref('leaderboard');
        let userName = cc.find("Account").getComponent("Account").userName; // get user's log in name
        let userScore = cc.find("Canvas/Main Camera/Dialog").getComponent("Dialog").totalPoint;
        let date = new Date();
        cc.log("userName: " + userName);  
        cc.log("userScore: " + userScore);

        leaderBoardRef.push().set({
            name: userName,
            score: userScore
        })
          
        var query = firebase.database().ref("userlist").orderByKey();
        query.once("value")
            .then(function(snapshot) {
                snapshot.forEach(function(childSnapshot) {
                    
                    var childData = childSnapshot.val();
                    var myupdate = { 
                        score: userScore,
                        date: date.toLocaleString()
                    }

                    if(childData.name == userName){
    
                        let ref = childSnapshot.ref.path.toString(); // this function will return /userlist/xxxxxxx
                        firebase.database().ref(ref).update(myupdate);
                    };
                    
                });
            });
    }

    losePlayerLife(){

        cc.director.getPhysicsManager().enabled = false;
        
        if(this.life == 1) this.gameOver();
        else {
            //this.life--;
            //cc.log("remain life: "+this.life);
            cc.find("Player Commander").getComponent("PlayerCommander").playerLife--;
            cc.log("world life--: " + cc.find("Player Commander").getComponent("PlayerCommander").playerLife);
            this.scheduleOnce(()=>{
                cc.director.loadScene("game_prepare");
            }, 2);
        }
    }

    // when player has not life, back to level select
    gameOver(){

        cc.log("---game over---")

        cc.audioEngine.playMusic(this.gameOverBGM, false);
        //cc.director.getPhysicsManager().enabled = false;

        cc.game.removePersistRootNode(cc.find("Player Commander"));

        cc.log("delete " + cc.find("Player Commander").name + " from persist root node");

        this.scheduleOnce(()=>{
            cc.audioEngine.stopMusic();
            cc.director.loadScene("level_select");
            
        }, 5);

    }
    gameClear_To_levelSelect(){
        
        cc.log("---back to level select scene---");
        cc.director.loadScene("level_select");
        cc.game.removePersistRootNode(cc.find("Player Commander"));
        cc.log("delete " + cc.find("Player Commander").name + " from persist root node");

        
    }
    

    update(dt){
        cc.find("Canvas/Main Camera/GameBoarder/Score/scorePoint").getComponent(cc.Label).string = String(this.score);
    }
}
