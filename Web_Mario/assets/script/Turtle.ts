const {ccclass, property} = cc._decorator;

@ccclass
export default class Turtle extends cc.Component {

    private right : boolean = true;

    private turtleVelocity : number = 40;

    private scaleX : boolean = true;


    onLoad () {
        cc.director.getPhysicsManager().enabled = true;
        this.node.scaleX = -1;
        this.node.getComponent(cc.RigidBody).linearVelocity = cc.v2(this.turtleVelocity*(1), 0);
    }
        
    start () {
        //this.node.getComponent(cc.Animation).play("Turtle_walk");
    }

    update (dt) {

    }

    onBeginContact(contact, self, other){
        
        var worldManifold = contact.getWorldManifold();
        var normal = worldManifold.normal;

        //cc.log(normal);

        if(other.node.name == "layer2" || other.node.name == "brick" || other.node.name == "Pipe" || other.node.name == "wood" || other.node.name == "upBrick" || other.node.name == "downBrick" || other.node.name == "upStair" || other.node.name == "clod" || (other.node.name == "layer1" && normal.y ==0)){
            
            if(this.right && normal.y == 0){

                cc.log("turn left");
                this.right = false;
                this.node.scaleX = 1;
                this.node.getComponent(cc.RigidBody).linearVelocity = cc.v2(this.turtleVelocity*(-1), 0);

            } else if (!this.right && normal.y == 0){

                this.right = true;
                this.node.getComponent(cc.RigidBody).linearVelocity = cc.v2(this.turtleVelocity*(1), 0);
                this.node.scaleX = -1;

            }            
        } else if (other.node.name == "Player" || other.node.name == "BigMario"){
            if(normal.y>0){
                // mario kill turtle
                cc.log("kill turtle");
                other.node.getComponent("myPlayer").stomp(); 
                // score
                cc.find("GameWorldManager").getComponent("myWorld").score += 200;
                self.node.destroy(); 
                
            } else { 
                //kill by turtle
                //cc.log('lost one life');
                other.node.getComponent("myPlayer").loseOneLife();
                
            }
            
        } else if (other.node.name == "floor"){
            self.node.destroy();
        } 
    }
}
