// Learn TypeScript:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/typescript.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/reference/attributes.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/life-cycle-callbacks.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/life-cycle-callbacks.html

const {ccclass, property} = cc._decorator;

@ccclass
export default class NewClass extends cc.Component {


    @property({ type: cc.AudioClip })
    private menuBGM: cc.AudioClip = null;

    private bgmVolume: number = null;

    // LIFE-CYCLE CALLBACKS:

    // onLoad () {}

    start () {
        if(!cc.audioEngine.isMusicPlaying()){
            //cc.log("player menu bgm");
            cc.audioEngine.setMusicVolume(0.5);
            var bgmid = cc.audioEngine.playMusic(this.menuBGM, false);
        }

        this.bgmVolume = cc.audioEngine.getVolume(bgmid);

        cc.log("run action");        
        //let action = cc.sequence(cc.moveBy(0.2, -5, 0), cc.moveBy(0.4, 10, 0), cc.moveBy(0.2, -5, 0)).repeatForever();
        let action = cc.sequence(cc.scaleBy(1./8., 1.1, 1.1), cc.scaleBy(4./8., 1/1.1, 1/1.1)).repeatForever();
        cc.find("Canvas/Title").runAction(action);

    }

    ToSignUp(){
        cc.log("---sign up---");
        cc.director.loadScene("signup");
        
    }

    ToSignIn(){
        cc.log("---log in---");
        cc.director.loadScene("login");
    }

    update (dt) {
        //cc.log(this.bgmVolume);
    }
}
