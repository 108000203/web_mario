const {ccclass, property} = cc._decorator;
 
@ccclass
export default class Mushroom extends cc.Component {
    
    private mushroomVelocity : number = 100;

    private right : boolean = true;
    
    @property({ type: cc.AudioClip })
    soundEffect: cc.AudioClip = null;


    onLoad() {
        cc.director.getPhysicsManager().enabled = true;

    }
    
    start(){

        this.scheduleOnce(function() { 
            this.node.getComponent(cc.RigidBody).linearVelocity = cc.v2(this.mushroomVelocity*(1), 0);
         }, 0.3);

    }
    public init(node: cc.Node)
    {
        this.setInitPos(node);
        //this.Move(node);
    }
 
    //this function sets the bullet's initial position when it is reused.
    private setInitPos(node: cc.Node)
    {
        this.node.parent = node.parent; 
        this.node.position = cc.v2(0, 16);   
        this.node.position = this.node.position.addSelf(node.position);
        
    }
 
    
    private Move(node: cc.Node)
    {

    }
    onBeginContact(contact, self, other){
        
        var worldManifold = contact.getWorldManifold();
        var normal = worldManifold.normal;
        /*
        if (other.node.name == "Player"){

            contact.disabled = true;
            cc.log("grow up");
            other.node.getComponent("myPlayer").marioLife ++;
            cc.log("after life: "+ other.node.getComponent("myPlayer").marioLife);
           
            cc.audioEngine.playEffect(this.soundEffect, false);
            
            cc.director.getPhysicsManager().enabled = false;
            other.node.getComponent("myPlayer").isUpgrate = true;

            let anmi = other.node.getComponent(cc.Animation);
            anmi.play("smToBig");

            cc.log("play");
            other.scheduleOnce(()=>{

                cc.log("stop");              
                anmi.stop("smToBig");
                cc.director.getPhysicsManager().enabled = true;         
                var prefab = cc.instantiate(other.node.getComponent("myPlayer").BigMarioPrefab);
                prefab.parent = cc.find("Canvas/PlayerCam");
                //cc.log(prefab.parent);
                prefab.position = new cc.Vec2(other.node.x, other.node.y);
                prefab.scaleX = other.node.scaleX;
                cc.log(prefab);
                other.node.destroy();
            
            }, 1);
            self.node.destroy();

        } else if (other.node.name == "BigMario"){

            cc.director.getPhysicsManager().enabled = false;
            other.node.getComponent("myPlayer").isUpgrate = true;
            cc.audioEngine.playEffect(this.soundEffect, false);
            let anmi = other.node.getComponent(cc.Animation);
            anmi.play("smToBig");
            other.scheduleOnce( ()=>{ 
                anmi.stop("smToBig"); 
                cc.director.getPhysicsManager().enabled = true;
            }, 1);
            self.node.destroy();
          */  
        // left and right
        if(other.node.name == "layer2" || other.node.name == "brick" || other.node.name == "Pipe" || other.node.name == "wood" || other.node.name == "upBrick" || other.node.name == "downBrick" || other.node.name == "upStair" || other.node.name == "clod"){
            
            if(this.right && normal.y == 0){
            
                this.right = false;
                this.node.getComponent(cc.RigidBody).linearVelocity = cc.v2(this.mushroomVelocity*(-1), 0);
      
            } else if (!this.right && normal.y == 0){
                  
                this.right = true;
                this.node.getComponent(cc.RigidBody).linearVelocity = cc.v2(this.mushroomVelocity*(1), 0);
      
            }
        } else if (other.node.name == "floor"){
            self.node.destroy();
        }
    }
    

}
 
