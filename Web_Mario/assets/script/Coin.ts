const {ccclass, property} = cc._decorator;
 
@ccclass
export default class Coin extends cc.Component {
 
    onLoad() {
        cc.director.getPhysicsManager().enabled = true;        	
    }
 
    public init(node: cc.Node) 
    {
        this.setInitPos(node);
        this.bulletMove();
    }
 
    //this function sets the bullet's initial position when it is reused.
    private setInitPos(node: cc.Node)
    {
        this.node.parent = node.parent; 
        this.node.position = cc.v2(0, 10);   
        this.node.position = this.node.position.addSelf(node.position);
    }
 
    //make the bullet move from current position
    private bulletMove()
    {
        let moveDir = null;
 
        // move bullet to 300 far from current position in 0.8s
       
         moveDir = cc.moveBy(0.8, 0, 100);
        
 
        let action = cc.spawn(moveDir, cc.fadeOut(0.8));
 
        let finished = cc.callFunc(() => {
            this.node.destroy();
        });
 
        this.scheduleOnce(() => {
            this.node.runAction(cc.sequence(action, finished));
        }); 

        cc.find("GameWorldManager").getComponent("myWorld").score += 100;

    }
}
 
