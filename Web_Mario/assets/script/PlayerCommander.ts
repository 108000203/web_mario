
const {ccclass, property} = cc._decorator;

@ccclass
export default class PlayerCommander extends cc.Component {

    // default player's stage level
    private playerLevel:number =1;

    // default player's lifepoint
    private playerLife:number = null;

    onLoad () {
        this.playerLife = 3; // default player's life point = 3
        cc.log("default player life = 3");
    }

    @property({ type: cc.AudioClip })
    private menuBGM: cc.AudioClip = null;

    start () {

        // add perminant Node
        //cc.game.removePersistRootNode(cc.find("Player Commander")); 
        cc.game.addPersistRootNode(this.node);
        cc.log("add " + this.node.name + " to persist root node");
        
        //cc.log(cc.audioEngine.isMusicPlaying())
        if(!cc.audioEngine.isMusicPlaying()){
            //cc.log("player menu bgm");
            cc.audioEngine.setMusicVolume(0.5);
            cc.audioEngine.playMusic(this.menuBGM, false);
        }
        //cc.log(cc.find("Account").getComponent("Account").userName);
        //cc.find("Canvas/Player/player name").getComponent(cc.Label).string;
        cc.find("Canvas/Player/player name").getComponent(cc.Label).string = cc.find("Account").getComponent("Account").userName;
        cc.find("Canvas/Date/recent online time").getComponent(cc.Label).string = cc.find("Account").getComponent("Account").userRecentOnlineTime;
        cc.find("Canvas/LastScore/score").getComponent(cc.Label).string = cc.find("Account").getComponent("Account").userLastScore;
    }

    

    StartLevel(event, level){
        //cc.log("event:", event, "data: ", level);
        if(level == 1){

            cc.log("---start level 01---");
            this.playerLevel = level; 
            cc.director.loadScene("game_prepare");

        } else if(level == 2){

            cc.log("---start level 02---");
            this.playerLevel = level;
            cc.director.loadScene("game_prepare");

        }
    }

    
       

    update (dt) {
        //cc.log("on playercommandar");
    }
}
