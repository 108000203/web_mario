// Learn TypeScript:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/typescript.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/reference/attributes.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/life-cycle-callbacks.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/life-cycle-callbacks.html

const {ccclass, property} = cc._decorator;

@ccclass
export default class PlayerCam extends cc.Component {

    

    @property(cc.Node)
    private mainCamera: cc.Node = null;

    // LIFE-CYCLE CALLBACKS:

    // onLoad () {}

    start () {

    }

    update (dt) {
        if(this.node.getChildByName("Player") == null){
            
            this.mainCamera.x = this.node.getChildByName("BigMario").x - cc.find("Canvas").getContentSize().width/2 + 50;
        } else {
            this.mainCamera.x = this.node.getChildByName("Player").x - cc.find("Canvas").getContentSize().width/2 + 50;

        }
        
        
    }
}
