
const {ccclass, property} = cc._decorator;

@ccclass
export default class AngelGoomba extends cc.Component {

    
    // LIFE-CYCLE CALLBACKS:

    // onLoad () {}

    start () {
        cc.director.getPhysicsManager().enabled = true;
        let action = cc.repeatForever(cc.sequence(cc.moveBy(3, 0, -100).easing(cc.easeInOut(2)), cc.delayTime(2), cc.moveBy(3, 0, 100).easing(cc.easeInOut(2)), cc.delayTime(2)));
        this.node.runAction(action);
    }

    // update (dt) {}

    onBeginContact(contact, self, other){
        
        var worldManifold = contact.getWorldManifold();
        var normal = worldManifold.normal;

        if (other.node.name == "Player" || other.node.name == "BigMario"){
            
            if(normal.y>0){
                // mario kill Goomba
                cc.log("kill AngelGoomba");
                other.node.getComponent("myPlayer").stomp();
                // score
                cc.find("GameWorldManager").getComponent("myWorld").score += 200;

                self.node.destroy(); 
                
            } else { 
                //kill by Goomba
                other.node.getComponent("myPlayer").loseOneLife();
                
            }

        }  
    }
}
