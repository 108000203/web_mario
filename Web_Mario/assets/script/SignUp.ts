
const {ccclass, property} = cc._decorator;

@ccclass
export default class SignUp extends cc.Component {

    public userEmail: string = null;

    public userName: string = "default";

    public userPassword: string = null;

    public flag : Boolean = false;

    

    // LIFE-CYCLE CALLBACKS:

    // onLoad () {}
    

    start () {
        //cc.game.addPersistRootNode(this.node);
        //cc.log("add " + this.node.name + " to persist root node"); // Node's name: PlayerMenuCommander 
    }


    SignUpWithEmail(){
        
        this.userEmail = this.node.getChildByName("InputTextbar_email").getComponent(cc.EditBox).textLabel.string;
        this.userName = this.node.getChildByName("InputTextbar_name").getComponent(cc.EditBox).textLabel.string;
        this.userPassword = this.node.getChildByName("InputTextbar_password").getComponent(cc.EditBox).textLabel.string;
        cc.log(this.userName); // log success
        cc.log('here');
        //cc.director.loadScene("menu");
        
        firebase.auth().createUserWithEmailAndPassword(this.userEmail, this.userPassword)
        .then((userCredential) => {

            // Signed in successful 
            
            var user = userCredential.user;
                  
            var ref = firebase.database().ref("userlist");
            var data = {
                email : this.userEmail,
                name : this.userName, // log nothing
                password : this.userPassword
            };
            ref.push(data);

            cc.find("Account").getComponent("Account").userName = this.userName;
            alert("sign up successful");
            cc.director.loadScene("level_select");
            
            
        })
        .catch((error) => {
            cc.director.loadScene("menu");
            var errorCode = error.code;
            var errorMessage = error.message;
            cc.log(errorMessage);
            alert(errorMessage);
            
            // ..
        });
    }


    LogInWithEmail(){
        
        this.userEmail = this.node.getChildByName("InputTextbar_email").getComponent(cc.EditBox).textLabel.string;
        this.userPassword = this.node.getChildByName("InputTextbar_password").getComponent(cc.EditBox).textLabel.string;

        //cc.director.loadScene("loading");
        
        
        firebase.auth().signInWithEmailAndPassword(this.userEmail, this.userPassword).then(function(result){
            
            cc.log("log-in-successful");
            
            var user = result.user;

            //alert("log in successful");
            Notification.requestPermission(function (status) {  
                var n = new Notification(user.email + " log-in successful");  
            }); 

            // load username
            var query = firebase.database().ref("userlist").orderByKey();
            query.once("value")
            .then(function(snapshot) {
                snapshot.forEach(function(childSnapshot) {
                    
                    var childData = childSnapshot.val();
                    
                    if(childData.email == user.email){
                        cc.find("Account").getComponent("Account").userName = childData.name;
                        cc.find("Account").getComponent("Account").userRecentOnlineTime = childData.date;
                        cc.find("Account").getComponent("Account").userLastScore = childData.score;
                        cc.director.loadScene("level_select");
                    };
                    
                });
            });
            

        }).catch(function(error) {

            //cc.log(cc.find("Canvas/LogInBlock/Input List"));
            //cc.find("Canvas/LogInBlock/Input List").getComponent("SignUp").flag = true;
            // Handle Errors here.
            var errorCode = error.code;
            var errorMessage = error.message;
            cc.log(errorMessage);
            alert(errorMessage);
            cc.director.loadScene("menu");
        });
        
    }

    
    //update (dt) {}
}
