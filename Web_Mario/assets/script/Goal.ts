
const {ccclass, property} = cc._decorator;

@ccclass
export default class Goal extends cc.Component {

    

    // LIFE-CYCLE CALLBACKS:

    // onLoad () {}

    start () {

    }
    onBeginContact(contact, self, other){
        
        if (other.node.name == "Player" || other.node.name == "BigMario"){

            cc.log("---Goal---");
            cc.find("GameWorldManager").getComponent("myWorld").gameClear();
            // cc.director.loadScene("game_finish");
        }
    }
    // update (dt) {}
}
