
const {ccclass, property} = cc._decorator;

@ccclass
export default class NewClass extends cc.Component {

    private tmpLevel:number = null;
    private tmpLife:number = null;

    // onLoad () {}

    start () {

        cc.audioEngine.stopMusic();

        //cc.log(cc.find("Player Commander").getComponent("PlayerCommander").playerLevel);
        this.tmpLevel = cc.find("Player Commander").getComponent("PlayerCommander").playerLevel;

        this.scheduleOnce(()=>{
            cc.director.loadScene("game_level_"+this.tmpLevel);
        }, 2)

        this.tmpLife = cc.find("Player Commander").getComponent("PlayerCommander").playerLife;
        cc.find("Canvas/lifepoint").getComponent(cc.Label).string = String(this.tmpLife);

    }

    // update (dt) {}
}
