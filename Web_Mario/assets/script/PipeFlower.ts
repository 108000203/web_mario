const {ccclass, property} = cc._decorator;

@ccclass
export default class PipeFlower extends cc.Component {

    
    // LIFE-CYCLE CALLBACKS:

    onLoad () {
        cc.director.getPhysicsManager().enabled = true;
    }

    start () {
        let action = cc.repeatForever(cc.sequence(cc.moveBy(1, 0, -34), cc.delayTime(2), cc.moveBy(1, 0, 34), cc.delayTime(2)));
        this.node.runAction(action);
    }

    onBeginContact(contact, self, other){
        
        var worldManifold = contact.getWorldManifold();
        var normal = worldManifold.normal;

        if (other.node.name == "Player" || other.node.name == "BigMario"){
            
            cc.log('lost one life');
            other.node.getComponent("myPlayer").loseOneLife();

        }  
    }

    // update (dt) {}
}
