
const {ccclass, property} = cc._decorator;

@ccclass
export default class NewClass extends cc.Component {

    public userEmail: string = null;

    public userName: string = "default";

    public userPassword: string = null;

    public userRecentOnlineTime: string = null;

    public userLastScore: string = null;


    // default player's stage level
    private playerLevel:number =1;

    // LIFE-CYCLE CALLBACKS:

    // onLoad () {}

    start () {
        cc.game.addPersistRootNode(this.node);
        cc.log("add " + this.node.name + " to persist root node"); // Node's name: PlayerMenuCommander 
    }

    // update (dt) {}
}
