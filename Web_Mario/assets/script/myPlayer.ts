const {ccclass, property} = cc._decorator;

@ccclass
export default class Player extends cc.Component 
{
    @property(cc.Prefab)
    private bulletPrefab: cc.Prefab = null;
    
    @property({ type: cc.AudioClip })
    private loseOneLifeBGM : cc.AudioClip = null;

    @property({ type: cc.AudioClip })
    private powerDownBGM : cc.AudioClip = null;

    @property({ type: cc.AudioClip })
    private stompBGM : cc.AudioClip = null;

    @property({ type: cc.AudioClip })
    private jumpBGM : cc.AudioClip = null;

    @property({ type: cc.AudioClip })
    private upgrateBGM: cc.AudioClip = null;

    @property()
    marioLife : number = 1; // up to 2

    private playerSpeed: number = 0;

    private zDown: boolean = false; // key for player to go left

    private xDown: boolean = false; // key for player to go right

    private jDown: boolean = false; // key for player to shoot

    private kDown: boolean = false; // key for player to jump

    public isDead: boolean = false;

    public isUpgrate: boolean = false;

    private upgrateTime: number = 1;

    private ableJump: boolean = false;

    private onGround: boolean = false;

    private canCreateBullet: boolean = true;

    private bulletInterval: number = 0.2; // timer interval for creating bullet

    private isReborn: boolean = false;

    private rebornTime: number = 0.4;

    private invisibleTime: number = 0.5;

    private visible: boolean = false;
    
    private jumpVelocity: number = 700; //

    private limitedVelocity: number = 300;

    @property(cc.SpriteFrame)
    private big_mario: cc.SpriteFrame = null;

    @property(cc.SpriteFrame)
    private small_mario: cc.SpriteFrame = null;

    @property(cc.Prefab)
    BigMarioPrefab: cc.Prefab = null;

    @property(cc.Prefab)
    SmallMarioPrefab: cc.Prefab = null;

    private anim: cc.Animation = null;
    
    private goldFinger_fly: boolean = false;
    

    

    onLoad() {

        cc.director.getPhysicsManager().enabled = true; 
        cc.director.getPhysicsManager().gravity = cc.v2(0, -200);      	
        cc.systemEvent.on(cc.SystemEvent.EventType.KEY_DOWN, this.onKeyDown, this);
        cc.systemEvent.on(cc.SystemEvent.EventType.KEY_UP, this.onKeyUp, this);

        this.anim = this.getComponent(cc.Animation);

        cc.director.getCollisionManager().enabled = true;
        cc.director.getCollisionManager().enabledDebugDraw = true;
        cc.director.getCollisionManager().enabledDrawBoundingBox = true;

        // physic system debug
        //cc.director.getPhysicsManager().debugDrawFlags = 
        //cc.PhysicsManager.DrawBits.e_aabbBit;
       //cc.PhysicsManager.DrawBits.e_pairBit |
        //cc.PhysicsManager.DrawBits.e_centerOfMassBit |
        //cc.PhysicsManager.DrawBits.e_jointBit;
        //cc.PhysicsManager.DrawBits.e_shapeBit;
        
    }

    start(){
        // can't be attack before invisible time
        this.scheduleOnce(()=>{ this.visible = true; }, this.invisibleTime);
    }

    onKeyDown(event) {
        //cc.log("Key Down: " + event.keyCode);
        //cc.log(cc.macro.KEY.f);

        var runAnim = this.node.getComponent(cc.Animation).getClips()[0].name;
        if(event.keyCode == cc.macro.KEY.z) {

            if(!this.zDown && !this.isUpgrate)this.anim.play(runAnim);
            this.zDown = true;
            this.xDown = false;
            
        } else if(event.keyCode == cc.macro.KEY.x) {
            
            if(!this.xDown && !this.isUpgrate)this.anim.play(runAnim);
            this.xDown = true;
            this.zDown = false;

            
        }else if(event.keyCode == cc.macro.KEY.k) {
            //cc.log('press k');
            this.kDown = true;
            
        }else if(event.keyCode == cc.macro.KEY.f) {

            cc.log("---flying mode---");
            if(!this.goldFinger_fly) this.goldFinger_fly = true;
            else this.goldFinger_fly = false;

        }
    }
    
    onKeyUp(event) {

        if(event.keyCode == cc.macro.KEY.z){
            this.zDown = false;
            if(!this.xDown && !this.isUpgrate)
                this.anim.play();
            
        }
            
        else if(event.keyCode == cc.macro.KEY.x){

            this.xDown = false;
            if(!this.zDown && !this.isUpgrate)
                this.anim.play();
            
        }
        else if(event.keyCode == cc.macro.KEY.j){

            this.jDown = false;
        }
        else if(event.keyCode == cc.macro.KEY.k){
            
            this.kDown = false;
            if((this.zDown || this.xDown) && !this.isUpgrate){
                
                var runAnim = this.node.getComponent(cc.Animation).getClips()[0].name;
                this.anim.play(runAnim);

            } else {
                if(!this.isUpgrate) this.anim.play();
            }
            
        }
            
    }
    
    private playerMovement(dt) {
        
        this.playerSpeed = 0;
        
        if(this.isDead && !this.isReborn) {

            cc.log("---reborn---");
            this.node.getComponent(cc.RigidBody).linearVelocity = cc.v2(0, 0);
            this.node.position = cc.v2(40, 40);
            this.isReborn = true;
            
            this.scheduleOnce(function(){
                this.isDead = false;
                //this.isReborn = false;
                
            }, this.rebornTime);
            return;
        }

        if(this.isUpgrate){
            
            this.scheduleOnce(()=>{
                cc.systemEvent.on(cc.SystemEvent.EventType.KEY_DOWN, this.onKeyDown, this);
                cc.systemEvent.on(cc.SystemEvent.EventType.KEY_UP, this.onKeyUp, this);
                this.isUpgrate = false;
            }, this.upgrateTime);
        }

        if(this.jDown && this.canCreateBullet) {
            this.createBullet();
        }

        if(this.zDown){
            
            
            this.playerSpeed = -200;
            this.node.scaleX = -1;
            
        }
        else if(this.xDown){
            
            this.playerSpeed = 200;
            this.node.scaleX = 1;
        }
        
        this.node.x += this.playerSpeed * dt;
        
        if(this.kDown && this.onGround)
            this.jump();
    }    

    private jump() {

        //cc.log("-jump-");
        if(!this.goldFinger_fly)
            this.onGround = false;
        this.getComponent(cc.RigidBody).linearVelocity = cc.v2(0, this.jumpVelocity);
        cc.audioEngine.playEffect(this.jumpBGM, false);
        //this.getComponent(cc.RigidBody).applyForceToCenter(cc.v2(0, this.jumpVelocity),false);
        let jumpAnim = this.node.getComponent(cc.Animation).getClips()[2].name;
        if(!this.isUpgrate){
            this.node.getComponent(cc.Animation).play(jumpAnim);
        }
            
        
    }

    private createBullet() {
        this.canCreateBullet = false;
        this.scheduleOnce(function(){
            this.canCreateBullet = true;
        }, this.bulletInterval);

        let bullet = cc.instantiate(this.bulletPrefab);
        bullet.getComponent('Bullet_ans').init(this.node);
    }
    
    update(dt) {
        this.playerMovement(dt);
     }

    onBeginContact(contact, self, other) {
        
        //cc.log("hit");
        contact.disabled = false;
        var worldManifold = contact.getWorldManifold();
        var normal = worldManifold.normal;
        
        if(other.node.name == "layer1") {

            //cc.log("hits the layer1");
            this.onGround = true;

        } else if(other.node.name == "layer2" || other.node.name == "brick" || other.node.name == "Pipe" || other.node.name == "wood" || other.node.name == "upBrick" || other.node.name == "downBrick" || other.node.name == "upStair" || other.node.name == "clod") {

            //cc.log("hits the layer2");
            if(normal.y < 0) this.onGround = true;

        } else if(other.node.name == "enemy" && !this.isReborn) {

            //cc.log("///hits the enemy///");
            this.isDead = true;

        } else if(other.node.name == "coinBox" || other.node.name == "mushroomBox"){

            //cc.log("hit questionBox");
            //cc.log(normal.y);
            if(normal.y < 0) this.onGround = true;      

        } else if (other.node.name == "AngelGoomba"){
            //cc.log("hit angelgoomba");
        } else if (other.node.name == "floor"){
            cc.log("hit floor, die");
            
            // 掉下去直接死亡
            cc.audioEngine.playMusic(this.loseOneLifeBGM, false)
            cc.systemEvent.off(cc.SystemEvent.EventType.KEY_DOWN, this.onKeyDown, this);
            cc.systemEvent.off(cc.SystemEvent.EventType.KEY_UP, this.onKeyUp, this);
            this.zDown = this.xDown = this.jDown = this.kDown = false;

            this.anim.play("SmallMario_dead");
            let dead_action = cc.sequence(cc.delayTime(0.5), cc.moveBy(0.5, 0, 50).easing(cc.easeOut(3)),  cc.moveTo(0.8, this.node.x, -100).easing(cc.easeIn(3)));
            this.node.runAction(dead_action);
            
            cc.find("GameWorldManager").getComponent("myWorld").losePlayerLife();
            
        } else if (other.node.name == "Turtle"){
            if(!this.visible){
                contact.disabled = true;
            }
        } else if (other.node.name == "Flower"){
            if(!this.visible){
                contact.disabled = true;
            }
        } else if (other.node.name == "Mushroom"){

            cc.systemEvent.off(cc.SystemEvent.EventType.KEY_DOWN, this.onKeyDown, this);
            cc.systemEvent.off(cc.SystemEvent.EventType.KEY_UP, this.onKeyUp, this);
            this.zDown = this.xDown = this.jDown = this.kDown = false;


            other.node.destroy();

            cc.log("grow up");
            
            contact.disabled = true;
            
            this.marioLife ++;

            cc.audioEngine.playEffect(this.upgrateBGM, false);
            
            cc.director.getPhysicsManager().enabled = false;
            
            this.isUpgrate = true;

            this.anim.play("smToBig");

            //cc.log("play");
            this.scheduleOnce(()=>{
       
                this.anim.stop("smToBig");
                cc.director.getPhysicsManager().enabled = true;         
                var prefab = cc.instantiate(this.BigMarioPrefab);
                prefab.parent = cc.find("Canvas/PlayerCam");
                //cc.log(prefab.parent);
                prefab.position = new cc.Vec2(self.node.x, self.node.y);
                prefab.scaleX = self.node.scaleX;
                //cc.log(prefab);
                self.node.destroy();
                cc.log(cc.find("Canvas/PlayerCam"))
            
            }, 1);
            
        }
    }
    loseOneLife(){

        // not vicible
        if(!this.visible) {

            return;
            
        }
        //死亡 
        //停留在原物件
        if(this.marioLife == 1){
            
            //this.scheduleOnce(()=>{ this.visible = true; }, this.invisibleTime);
            cc.log(">>lose one life>>");

            this.marioLife = 0;
            
            cc.audioEngine.playMusic(this.loseOneLifeBGM, false);

            // freeze objects' motions
            cc.systemEvent.off(cc.SystemEvent.EventType.KEY_DOWN, this.onKeyDown, this);
            cc.systemEvent.off(cc.SystemEvent.EventType.KEY_UP, this.onKeyUp, this);
            this.zDown = this.xDown = this.jDown = this.kDown = false;
           
            // death animation
            //cc.log("run dead action");
            this.anim.play("SmallMario_dead");
            let dead_action = cc.sequence(cc.delayTime(0.5), cc.moveBy(0.5, 0, 20).easing(cc.easeOut(3)),  cc.moveTo(0.8, this.node.x, -100).easing(cc.easeIn(3)));
            this.node.runAction(dead_action);

            
            // player's life --
            cc.find("GameWorldManager").getComponent("myWorld").losePlayerLife();
            

        // 更換成SmallMario物件
        } else if (this.marioLife == 2){

            this.visible = false;
            cc.log(">>powerdown>>");
            this.marioLife--;
            let anmi = this.node.getComponent(cc.Animation);
            
            cc.audioEngine.playEffect(this.powerDownBGM, false);

            anmi.play("smToBig");
            this.scheduleOnce(()=>{  

                anmi.stop("smToBig");         
                var prefab = cc.instantiate(this.SmallMarioPrefab);
                prefab.parent = cc.find("Canvas/PlayerCam");
                prefab.position = new cc.Vec2(this.node.x, this.node.y);
                prefab.scaleX = this.node.scaleX;
                this.node.destroy();
              
            }, 0.5); // wait animate over

            
        }      
    }
    stomp(){

        cc.audioEngine.playEffect(this.stompBGM, false);
        //let stompaction = cc.moveBy(0.2, 0, 120);
        this.node.getComponent(cc.RigidBody).linearVelocity = cc.v2(0, 500);
        //this.node.runAction(stompaction);
    }
}