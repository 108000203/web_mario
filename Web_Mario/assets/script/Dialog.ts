
const {ccclass, property} = cc._decorator;

@ccclass
export default class Dialog extends cc.Component {

    @property({type:cc.Node, tooltip:"弹出式对话框的遮罩节点"})
    mask : cc.Node = null;

    @property({type:cc.Node, tooltip:"弹出式对话框的主体内容节点"})
    content : cc.Node = null;

    @property({tooltip:"弹出式对话框初始化时的透明度"})
    maskOpacity : number = 200;

    public totalPoint: number = null;

    // LIFE-CYCLE CALLBACKS:

    onLoad () {
        this.node.active = false;
        this.mask.opacity = this.maskOpacity;
    }

    start () {

    }

    showDialog(){
        cc.log("show dialog");

        this.node.active = true;

        cc.log(this.content.getComponent("myWorld").timer);
        let leftTime = this.content.getComponent("myWorld").timer;
        cc.find("Canvas/Main Camera/Dialog/Timer/leftTime").getComponent(cc.Label).string = String(leftTime);
        
        let score = this.content.getComponent("myWorld").score;
        cc.log(leftTime + ", " + score);
        this.totalPoint = leftTime*score;
        cc.find("Canvas/Main Camera/Dialog/Score/point").getComponent(cc.Label).string = String(score);
        
        cc.find("Canvas/Main Camera/Dialog/Multiple").getComponent(cc.Label).string = String(leftTime) + "x" + score + "=" + this.totalPoint;
        
        
        

        // mask淡入`

        this.mask.opacity = 0;

        let fIn : cc.Action = cc.fadeTo(0.1, this.maskOpacity);

        this.mask.runAction(fIn);
    }
    
    showLeaderBoard(){
        cc.log("show leaderboard");
        
        this.node.active = true;

        // mask淡入
        //this.mask.opacity = 0;

        //let fIn : cc.Action = cc.fadeTo(0.1, this.maskOpacity);

        //this.mask.runAction(fIn);
        var str_before_name = "<color=white>";
        var str_after_name = "</c><br/>";
        let n = 10;
        var ref = firebase.database().ref("leaderboard").orderByChild("score").limitToLast(10).once('value', function (snapshot) {
            snapshot.forEach(function (item) {

                cc.log("name: " + item.val().name + ", score: " + item.val().score);
                let namelen = item.val().name.length;
                var pnumber = '        ';
                var namestr = item.val().name + pnumber;
                //cc.log("name: " + item.val().name);
                //cc.log("my slice: " + namestr.slice(0, 7));
                //cc.log(cc.find("Canvas/Main Camera/Leader Board/namelist"));
                
                cc.find("Canvas/Main Camera/Leader Board/namelist").getComponent(cc.RichText).string =
                str_before_name  + namestr.slice(0,7) + item.val().score + str_after_name 
                + cc.find("Canvas/Main Camera/Leader Board/namelist").getComponent(cc.RichText).string;
            
                
            })
        });
          
    }

    closeLeaderBoard(){

        cc.find("Canvas/Main Camera/Leader Board/namelist").getComponent(cc.RichText).string = "";

        this.node.active = false;

        let fIn : cc.Action = cc.fadeTo(0.1, 0);

        this.node.runAction(fIn);
    }

    
    // update (dt) {}
}
