const { ccclass, property } = cc._decorator;

@ccclass
export default class questionBox extends cc.Component {

  @property({ type: cc.AudioClip })
  soundEffect: cc.AudioClip = null;

  @property(cc.Prefab)
  private itemPrefab: cc.Prefab = null;
  
  protected isTouched: boolean = false;

  private anim: cc.Animation = null;

  private animState: cc.AnimationState = null;

  private highestPos: number = 118;

  private moveSpeed: number = 100;

  private springVelocity: number = 320;

  @property(cc.SpriteFrame)
  private Sprite2 : cc.SpriteFrame = null;

  start() {
    //this.anim = this.getComponent(cc.Animation);
    /*
    if (this.node.name == "Conveyor") {
      this.node.scaleX = Math.random() >= 0.5 ? 1 : -1;
      this.moveSpeed *= this.node.scaleX;
    }
    */
    this.getComponent(cc.PhysicsBoxCollider).enabled = true;
  }

  reset() {
    this.isTouched = false;
  }

  update(dt) {
      
  }

  playAnim() {
    if (this.anim) this.animState = this.anim.play();
  }

  getAnimState() {
    if (this.animState) return this.animState;
  }

  platformDestroy() {
    
    this.node.destroy();
  }


  onBeginContact(contact, self, other){
    
    var worldManifold = contact.getWorldManifold();
    var normal = worldManifold.normal;
    
    if(normal.y < 0 && !this.isTouched){
      
      if(self.node.name == "coinBox"){

        this.isTouched = true;
        cc.log("hit coin box");
        this.popCoin();
        cc.audioEngine.playEffect(this.soundEffect, false);

        var anim = this.getComponent(cc.Animation);
        anim.pause("questionBox");
        this.changeSprite();

      } else if(self.node.name == "mushroomBox"){

        this.isTouched = true;
        cc.log("hit mushroom box");
        this.popMushroom();
        cc.audioEngine.playEffect(this.soundEffect, false);

        var anim = this.getComponent(cc.Animation);
        anim.pause("questionBox");
        this.changeSprite();

      }
    }
  }
  
  onPreSolve(contact, self, other){
    if(self.node.name == "Conveyor"){
      other.getComponent(cc.RigidBody).linearVelocity = cc.v2(this.moveSpeed, 0);
    }
  }
  onEndContact(contact, self, other){
    // prevent extra force when falling from Conveyor
    if(self.node.name == "Conveyor"){
      other.getComponent(cc.RigidBody).linearVelocity = cc.v2(0, 0);
    }
  }
  popCoin(){
    cc.log("pop coin!");
     
    let item = cc.instantiate(this.itemPrefab);
    
    item.getComponent('Coin').init(this.node);
    
  }
  popMushroom(){
    cc.log("pop mushroom!");
     
    let item = cc.instantiate(this.itemPrefab);
    
    item.getComponent('Mushroom').init(this.node);
    
  }
  changeSprite(){
    this.node.getComponent(cc.Sprite).spriteFrame = this.Sprite2;
  }



}
